# Presto Card Balance Checker

A simple Python script (paired with a PHP script to send emails) to check your Presto Card balance and notify you if it is below a certain amount.

0. By default this script is set to call prestorefill.php if your balance is below the set minimum ($20); you can change this to however you'd like to receive alerts (or not)
1. chmod presto.py as 755 (make it executable)
2. Execute it: python3 presto.py
3. Observe that the cookie file should now be populated with some session content

If all works, optionally you can create a cronjob to run this on a schedule (e.g. 0 12 * * * python3 /home/presto/presto.py)
