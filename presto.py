import requests
import bs4
import re
import http.cookiejar

#Settings
thresh = 20 # dollar amount to alert you if balance is less than
cookiefile = '/home/presto/cookie'
ua = 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36'

cj = http.cookiejar.LWPCookieJar(cookiefile)
cj.load(ignore_discard=True) # comment out when running this for the first time
session = requests.session()
session.cookies = cj
headers = {
	'Origin': 'https://www.prestocard.ca',
	'Referer': 'https://www.prestocard.ca/en',
	'User-Agent': ua
}
try:
	page = session.get('https://www.prestocard.ca/en/dashboard', headers=headers, cookies=cj, timeout=5)
	soup = bs4.BeautifulSoup(page.text)
	token = soup.select('form#signwithaccount input[name="__RequestVerificationToken"]')
	if token:
		tokenval = token[0]['value']
		data = {
			'__RequestVerificationToken': tokenval,
			'Login': 'prestousername',
			'Password': 'prestopassword'
		}
		session.post('https://www.prestocard.ca/api/sitecore/AFMSAuthentication/SignInWithAccount', data=data, headers=headers, cookies=cj, timeout=5)
		cj.save(cookiefile, ignore_discard=True)
		page = session.get('https://www.prestocard.ca/en/dashboard', headers=headers, cookies=cj, timeout=5)
	matches = re.findall(r"\"Balance\":([^,]+)", page.text)
	if matches:
		#print(matches) # uncomment if you have more than one card on your account (lost/old cards are also included)
		presto = float(matches[0])
		prestostr = "{0:.2f}".format(presto)
		print('Current Balance: $'+prestostr)
		if presto < thresh:
			print('pinging refill email')
			headers = {
				'Origin': 'https://www.yourdomain.com',
				'Referer': 'https://www.yourdomain.com/',
				'User-Agent': ua
			}
			requests.get(url='https://www.yourdomain.com/prestorefill.php?presto='+prestostr+'&thresh='+str(thresh), headers=headers, timeout=5)
		else:
			print('Card above threshold')
except requests.exceptions.Timeout:
	print('timed out')
